<?php
function next_prev() {
  $next_post = get_adjacent_post( true,'',false );
  $prev_post = get_adjacent_post( true,'',true );
  

  if( isset($prev_post->ID) && isset($next_post->ID) ) {
    print '<div class="prev-next-container">
    <a href="' . get_the_permalink($prev_post->ID) .'">
    <div class="prev" style="background-image: url(' . get_the_post_thumbnail_url($prev_post->ID) . ')">
    <div class="content"><div class="hover-state"></div><div class="text"><h3>View Previous</h3><p>' . get_the_title($prev_post->ID) . '</p></div></div>
  </div>
    </a>
    <a href="' . get_the_permalink($next_post->ID) .'">
    <div class="next" style="background-image: url(' . get_the_post_thumbnail_url($next_post->ID) . ')">
    <div class="content"><div class="hover-state"></div><div class="text"><h3>View Next</h3><p>' . get_the_title($next_post->ID) . '</p></div></div>
  </div>
    </a>
    </div>
    ';
  } else if(! isset($prev_post->ID) && isset($next_post->ID) ) {
    print '<div class="prev-next-container">
    <a href="' . get_the_permalink($next_post->ID) .'">
    <div class="next" style="background-image: url(' . get_the_post_thumbnail_url($next_post->ID) . ')">
    <div class="content"><div class="hover-state"></div><div class="text"><h3>View Next</h3><p>' . get_the_title($next_post->ID) . '</p></div></div>
  </div>
    </a>
    </div>
    ';

  } else {
    print '<div class="prev-next-container">
    <a href="' . get_the_permalink($prev_post->ID) .'">
    <div class="next" style="background-image: url(' . get_the_post_thumbnail_url($prev_post->ID) . ')">
      <div class="content"><div class="hover-state"></div><div class="text"><h3>View Previous</h3><p>' . get_the_title($prev_post->ID) . '</p></div></div>
    </div>
    </a>
    </div>
    ';

  }
}
